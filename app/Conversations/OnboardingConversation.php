<?php

namespace App\Conversations;

use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use App\article;
use App\TelegramUser;

class OnboardingConversation extends Conversation
{

    protected $name;
    protected $depth;
    protected $schedule;
        
    public function askName()
    {

        $this->ask('Wie heißt du?', function (Answer $answer) {
            $this->name = $answer->getText();
            $this->say('Sehr erfreulich '.$this->name);
            $this->bot->userStorage()->save([
                'name' => $this->name
            ]);

            $this->askHowMany();
            
        });

        

    }

    public function askDepth()
    {
        $question = Question::create('Wie tief möchtest du ins Thema einsteigen?')
                  ->addButtons([
                      Button::create('Nur einen Überblick')->value(1),
                      Button::create('Gib mir alles!')->value(2),
                  ]);
        return $this->ask($question, function (Answer $answer) {
            $this->depth = $answer->getValue();
            $this->say('Du möchest Paket '.$this->depth);
            $this->askHowMany();
        });

    }


    public function askHowMany() {
        $question = Question::create("Wie viel Zeit pro Tag hast du?")
                  ->addButtons([
                      Button::create('5 Minuten')->value("0"),
                      Button::create('10 Minuten')->value("0,5"),
                      Button::create('15 Minuten')->value("0,5,10"),
                      Button::create('20 Minuten')->value("0,5,10,15"),
                      Button::create('25 Minuten')->value("0,5,10,15,20"),
                  ]);
        return $this->ask($question, function (Answer $answer){
            $this->schedule = $answer->getValue();
            #$this->say('Ok, ich werde dir immer so gegen '.$this->schedule.' was zum Lesen schicken.');
            $this->askSchedule();
        });


    }


    
    

    public function askSchedule() {
        $question = Question::create("Wann hast du Zeit?")
                  ->addButtons([
                      Button::create('Morgens')->value($this->schedule." 8"),
                      Button::create('Mittags')->value($this->schedule." 12"),
                      Button::create('Abends')->value($this->schedule." 18"),
                  ]);
        return $this->ask($question, function (Answer $answer){
            $this->schedule = $answer->getValue();
            #$this->say('Ok, ich werde dir immer so gegen '.$this->schedule.' was zum Lesen schicken.');
            $this->askScheduleDays();
        });


    }


        

    
    
    public function askScheduleDays() {
        $question = Question::create("Auch am Wochenende?")
                  ->addButtons([
                      Button::create('Nein')->value($this->schedule." * * 1-5"),
                      Button::create('Ja')->value($this->schedule." * * *"),
                      Button::create('Nur am Wochenende!')->value($this->schedule." * * 0,5,6"),
                      Button::create('Ich muss testen!')->value("*/2 * * * *")
                  ]);
        return $this->ask($question, function (Answer $answer){
            $this->schedule = $answer->getValue();
            #$this->say('Ok, ich werde dir immer so gegen '.$this->schedule.' was zum Lesen schicken.');
            $this->saveToDB();
        });


    }


    

    public function saveToDB(){
        #$this->say("Speichere Daten...");
        $users = \App\TelegramUser::updateOrCreate(
            ['id'=> $this->bot->getUser()->getId()],
            [ 'name' => $this->name,
             'depth' => 1, #
            'schedule' => $this->schedule,
            'article_state' => 1]
        );
        $this->say("Alles klar :-)");
    }

    
    
    public function run()
    {
        $user = $this->bot->getUser();
        #$this->say('Hi, deine UserID ist: '. $user->getId());
        $this->say('Hi, ich bin publibot!');
        $this->askName();
 

    }
}


