<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use BotMan\Drivers\Telegram\TelegramDriver;
use App\TelegramUser;
use App\article;


use BotMan\BotMan\Messages\Attachments\Video;
use BotMan\BotMan\Messages\Attachments\Image;
use BotMan\BotMan\Messages\Outgoing\OutgoingMessage;


class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $botman;
    
    protected $commands = [
        Commands\botmanbroadcastall::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */

    

    protected function schedule(Schedule $schedule)
    {
        $this->botman=app('botman');
        $users = TelegramUser::all();
        foreach($users as $user){

            $state = $user->article_state;
            $depth = $user->depth;
            $my_article =  article::where('depth','<=', $depth)
                        ->orderBy('order')
                        ->skip($state-1)
                        ->first();

            $schedule->call(function($user, $article, $state){
                
                #$this->botman->say($article->content,
                #                   $user->id , TelegramDriver::class);
                #$attachment = new Image('https://res.cloudinary.com/sagacity/image/upload/c_crop,h_1000,w_1500,x_0,y_0/c_limit,dpr_auto,f_auto,fl_lossy,q_80,w_1200/Kitten_murder_Jeff_Merkley_2_copy_hdpoxd.jpg');
                if ($article->type == 2){
                    $attachment = new Image($article->link);
                    $message = OutgoingMessage::create($article->content)->withAttachment($attachment);
                    $this->botman->say($message,$user->id , TelegramDriver::class);
                }
                elseif ($article->type == 1){
                    $this->botman->say($article->content,
                                   $user->id , TelegramDriver::class);
                }
                elseif ($article->type == 3){
                    $attachment = new Video($article->link);
                    $message = OutgoingMessage::create($article->content)->withAttachment($attachment);
                    $this->botman->say($message,$user->id , TelegramDriver::class);

                }

                    
                $user->article_state = $state+1;
                $user->save();
            },  [$user, $my_article, $state])
                     ->timezone('Europe/Berlin')
                     ->cron($user->schedule);
                               
        }
    }

    #486373399 (MKM) 298498514 (AM)
    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
